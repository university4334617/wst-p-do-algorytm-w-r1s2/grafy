import networkx as nx
import matplotlib.pyplot as plt
import random
l_wierzcholkow = 10
prawd = 0.2

#Zadanie 1
def DFS(graf, wierzcholek, numer_ss, lista_ss): #Przeszukiwanie w głąb: https://eduinf.waw.pl/inf/alg/001_search/0125.php
    if lista_ss[wierzcholek] == 0: #czy odwiedzony
        lista_ss[wierzcholek] = numer_ss #jeśli nie to dostaje numer bierzącej spojnej skladowej (jest != 0)
        for sasiad in graf[wierzcholek]:
            DFS(graf, sasiad, numer_ss, lista_ss)

def znajdz_ss(graf):
    l_wierzch = len(graf)
    lista_ss = [0] * l_wierzch
    liczba_ss = 0

    for w in range(l_wierzch):
        if lista_ss[w] == 0: #musi być 2 razy sprawdzane, bo inaczej wywala mnóstwo figur pustych
            liczba_ss += 1
            DFS(graf, w, liczba_ss, lista_ss)

    return lista_ss, liczba_ss

def wyswietl_graf_zad1(graf, tytul):
    plt.figure()
    pozycje_wierz = nx.spring_layout(graf) #Obliczanie (na podstawie algorytmu rozkładu wiosennego) pozycji wierzcholkow (polaczone - blisko, niepol - daleko)
    random_color = '#' + ''.join(random.choices('0123456789ABCDEF', k=6))
    nx.draw(graf, pozycje_wierz, with_labels=True, node_color=random_color, node_size=700, font_size=12, font_weight='bold')
    plt.title(tytul)
    plt.show()

def grupowanie_wierzcholkow(lista_ss, ile_jest_ss):
    pogrupowane_wierz = [[] for _ in range(ile_jest_ss)]  #lista list, która będzie zawierać listy wierzcholkow dla danych ss
    for w in range(len(lista_ss)):
        pogrupowane_wierz[lista_ss[w] - 1].append(w)
    return pogrupowane_wierz

#Graf początkowy
G1 = nx.gnp_random_graph(l_wierzcholkow, prawd)
wyswietl_graf_zad1(G1, "Graf przed identyfikacją spójnych składowych - zadanie 1")

#Sprawdzamy spojne skladowe
lista_ss, ile_jest_ss = znajdz_ss(G1)
if ile_jest_ss == 1:
    print("Graf jest spójny.")
else:
    print("Graf zawiera {} spójne składowe.".format(ile_jest_ss))

#Grupujemy wierzholki w spojnych skladowych
pogrupowane_wierz = grupowanie_wierzcholkow(lista_ss, ile_jest_ss)
for ss, wierzcholki in enumerate(pogrupowane_wierz):
    print("Wierzchołki składowej {}: {}".format(ss + 1, wierzcholki))

#Tworzymy oddzielne zbiory wierzcholkow dla kazdej spojnej skladowej
lista_zbiorow_w = [nx.Graph() for _ in range(ile_jest_ss)]
for w in range(len(lista_ss)):
    lista_zbiorow_w[lista_ss[w] - 1].add_node(w)

#Wyświetlanie grafów spójnych składowych
for ss in range(ile_jest_ss):
    wyswietl_graf_zad1(lista_zbiorow_w[ss], "Spójna składowa {}".format(ss+1))



#Zadanie 2
def dijkstra(graf, start, end): #https://eduinf.waw.pl/inf/alg/001_search/0138.php
    S = set() #zbior wierzcholkow rozwazonych
    Q = set(graf.nodes()) #zbior wierzcholkow nierozwazonych
    d = {wierzcholek: float('inf') for wierzcholek in graf.nodes()} #tablica wag
    d[start] = 0
    p = {wierzcholek: -1 for wierzcholek in graf.nodes()} #tablica poprzednikow
    while Q:
        u = min(Q, key=lambda wierzcholek: d[wierzcholek])
        Q.remove(u)
        S.add(u)
        for w in graf.neighbors(u):
            if w not in Q:
                continue

            if d[w] > d[u] + graf[u][w]['weight']: #aktualzujemy odleglosc sasiada, jesli obliczona jest mniejsza niz aktualna
                d[w] = d[u] + graf[u][w]['weight']
                p[w] = u

    # Konstrukcja ścieżki
    sciezka_wynik = []
    current = end
    while current != -1:
        sciezka_wynik.append(current)
        current = p[current]
    sciezka_wynik.reverse()
    return d[end], sciezka_wynik

def wyswietl_graf_zad2(graf, tytul):
    pozycje_wierz = nx.spring_layout(graf)
    random_color = '#' + ''.join(random.choices('0123456789ABCDEF', k=6))
    nx.draw(graf, pozycje_wierz, with_labels=True, node_color=random_color, node_size=700, font_size=12, font_weight='bold')
    weight = nx.get_edge_attributes(graf, 'weight')
    nx.draw_networkx_edge_labels(graf, pozycje_wierz, edge_labels=weight)
    plt.title(tytul)
    plt.show()

#Tworzenie grafu
G2 = nx.gnp_random_graph(l_wierzcholkow, prawd)
for (u, v, w) in G2.edges(data=True):
    w['weight'] = random.randint(1, 10)

#Wybór wierzcholkow startowych
start_w = random.randint(0, l_wierzcholkow-1)
end_w = random.randint(0, l_wierzcholkow-1)
while start_w == end_w:
    end_w = random.randint(0, l_wierzcholkow-1)

#Dijkstra
Dlugosc_wynik, sciezka_wynik = dijkstra(G2, start_w, end_w)
if Dlugosc_wynik == float('inf'):
    print("Nie istnieje sciezka miedzy wierzcholkami {} i {}.".format(start_w, end_w))
else:
    print("Najkrotsza sciezka miedzy wierzcholkami {} i {}: {}".format(start_w, end_w, sciezka_wynik))
    print("Najkrotsza sciezka: ", Dlugosc_wynik)

#Wyświetlanie grafu
wyswietl_graf_zad2(G2, "Graf z wagami - zadanie 2")
plt.close()



#Zadanie 3
class ZbiorRozlaczny:
    #Ranga - liczba poziomów drzewa dla zbioru
    def __init__(self, n):
        self.parent = [w for w in range(n)] #Każdy element na początku to własny zbiór
        self.rank = [0] * n  

    def find(self, u): #Znajdujemy zbiór do którego należy u
        if self.parent[u] != u:
            self.parent[u] = self.find(self.parent[u])
        return self.parent[u]

    def union(self, u, v): #Łączymy 2 zbiory
        u_root = self.find(u)
        v_root = self.find(v) #Szukamy korzeni

        if u_root == v_root: #Muszą należeć do tego samgo zbioru
            return False

        if self.rank[u_root] > self.rank[v_root]:
            self.parent[v_root] = u_root
        elif self.rank[u_root] < self.rank[v_root]:
            self.parent[u_root] = v_root
        else:
            self.parent[v_root] = u_root
            self.rank[u_root] += 1

        return True #Nastąpiła zmiana
    
def kruskal(graf): #https://pl.wikipedia.org/wiki/Algorytm_Kruskala #Musi być spójny graf
    krawedzie = [(weight, u, v) for u, v, weight in graf.edges(data='weight')]
    krawedzie.sort() #sortujemy krawedzie po wagach

    n = len(graf.nodes())
    mdr = nx.Graph()
    ds = ZbiorRozlaczny(n) #zbiór rozłączny

    for weight, u, v in krawedzie: #sprawdzamy, czy polączenie ze soba danych wierzcholkow nie spowoduje cyklu w mdr - jeśli nie to dodajemy do struktury
        if ds.union(u, v):
            mdr.add_edge(u, v, weight=weight)

    return mdr

def prim(graf): #https://pl.wikipedia.org/wiki/Algorytm_Prima
    start_w = list(graf.nodes())[0] #pierwszy w -> startowy
    odwiedzone_w = set([start_w])
    krawedzie = [(weight, start_w, v) for v, weight in graf[start_w].items()] #lista w incydentnych do w startowego
    mdr = nx.Graph()

    while krawedzie:
        krawedzie = sorted(krawedzie, key=lambda x: x[0]['weight'])  #Sortowanie listy krotek wg wag krawędzi
        weight, u, v = krawedzie.pop(0)  #Bierzemy tą o najmniejszej wadze i  usuwamy z listy krotek
        if v not in odwiedzone_w:
            odwiedzone_w.add(v)
            mdr.add_edge(u, v, weight=weight['weight'])
            for next_v, next_weight in graf[v].items():
                if next_v not in odwiedzone_w:
                    krawedzie.append((next_weight, v, next_v))

    return mdr

def wyswietl_graf_zad_3(graf, tytul, cos):
    plt.subplot(1, 2, cos)
    pozycje_wierz = nx.spring_layout(graf)
    random_color = '#' + ''.join(random.choices('0123456789ABCDEF', k=6))
    nx.draw(graf, pozycje_wierz, with_labels=True, node_color=random_color, node_size=700, font_size=12, font_weight='bold')
    weight = nx.get_edge_attributes(graf, 'weight')
    nx.draw_networkx_edge_labels(graf, pozycje_wierz, edge_labels=weight)
    plt.title(tytul)

#Tworzenie grafu
G3 = nx.gnp_random_graph(l_wierzcholkow, 0.4)
for (u, v, w) in G3.edges(data=True):
    w['weight'] = random.randint(1, 10)

#Wyświetlanie grafu pocz vs Kruskala
plt.figure(figsize=(12, 5))
wyswietl_graf_zad_3(G3, "Graf poczatkowy", 1)
kruskal_po = kruskal(G3) #Kruskal
wyswietl_graf_zad_3(kruskal_po, "Minimalne drzewo rozpinające (Kruskal)", 2)
plt.tight_layout()
plt.show()

#Wyświetlanie grafu pocz vs Prima
plt.figure(figsize=(12, 5))
wyswietl_graf_zad_3(G3, "Graf poczatkowy", 1)
prim_po = prim(G3) #Prim
wyswietl_graf_zad_3(prim_po, "Minimalne drzewo rozpinające (Prim)", 2)
plt.tight_layout()
plt.show()
